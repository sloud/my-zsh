# `nvm` plugin

Ensures [`nvm`](https://github.com/nvm-sh/nvm) is installed and configured in your shell. As well as, installing the latest node lts version.