lsports() {
    printf "%-20s%-20s%-20s%-20s\n" "Host" "Port" "Process" "PID"
    printf "%-20s%-20s%-20s%-20s\n" "----" "----" "-------" "---"

    sudo ss -tulpn |
        gawk '/users:\(\(/ {
            match($5, "(.+):([0-9]+)", host)
            hostname = host["1"]
            port = host["2"]
            details = $7
            sub("users:\\(\\(", "", details)
            sub("\\)\\)", "", details)
            split(details, deets, ",")
            match(deets["2"], "pid=(.*)", pid)
            match(deets["1"], "\"(.*)\"", proc)

            printf("%-20s%-20s%-20s%-20s\n", hostname, port, proc["1"], pid["1"])
        }' |
        sort -nk2
}
