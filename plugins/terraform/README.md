# `terraform` plugin

Ensures [`terraform`](https://developer.hashicorp.com/terraform) is install in your shell.

```sh
antigen bundle https://gitlab.com/sloud/my-zsh.git@main plugins/terraform

# for autocompletion
antigen bundle terraform
```