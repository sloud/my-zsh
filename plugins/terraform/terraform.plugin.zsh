install_terraform() {
    local whereami="$1"
    local bin="$HOME/.local/bin"
    local expected_hash='0ddc3f21786026e1f8522ba0f5c6ed27a3c8cc56bfac91e342c1f578f8af44a8'
    local terraform_zip="$whereami/terraform.zip"
    local terraform_bin="$whereami/terraform"

    if [ ! -f "$terraform_zip" ] && ! type terraform 2>&1 >/dev/null; then
        mkdir -p "$bin"
        curl "https://releases.hashicorp.com/terraform/1.6.0/terraform_1.6.0_linux_amd64.zip" -o "$terraform_zip"
        
        local actual_hash="$(sha256sum "$terraform_zip" | awk '{ print $1 }')"

        if [[ "$actual_hash" != "$expected_hash" ]]; then
            echo 'error: terraform hash mismatch. terraform was not installed'
            echo "=> expected: $expected_hash"
            echo "=> actual  : $actual_hash"
            return 1
        fi

        unzip "$terraform_zip" -d "$whereami"
        cp -v "$terraform_bin" "$bin/terraform"
    fi
}

install_terraform "${0:A:h}"
unset install_terraform
