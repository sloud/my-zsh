export PYENV_ROOT="${PYENV_ROOT:-"$HOME/.pyenv"}"
export PYENV_VERSION="${PYENV_DEFAULT_VERSION:-"3.11.4"}"

install_pyenv() {
    local whereami="$1"
    local bin="$PYENV_ROOT/bin"

    path+=($bin)

    if [ ! -x "$bin/pyenv" ]; then
        curl https://pyenv.run | bash
    fi

    eval "$(pyenv init -)"

    # Install the global python version if it is not currntly installed
    if [[ ! "$(pyenv versions --bare)" =~ (^|[[:space:]])$PYENV_VERSION($|[[:space:]]) ]]; then
        echo "==> installing python version: $PYENV_VERSION"
        pyenv install "$PYENV_VERSION"
    fi
}

install_pyenv "${0:A:h}"
unset install_pyenv
