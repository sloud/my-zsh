install_pnpm() {
    local whereami="$1"
    local bin="$HOME/.local/bin"

    if ! type pnpm 2>&1 >/dev/null; then
        mkdir -p "$bin"
        curl -fsSL "https://github.com/pnpm/pnpm/releases/latest/download/pnpm-linuxstatic-x64" -o "$bin/pnpm"
        chmod +x "$bin/pnpm"
    fi
}

install_pnpm "${0:A:h}"
unset install_pnpm
