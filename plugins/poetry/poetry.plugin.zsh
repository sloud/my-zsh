install_poetry() {
    local whereami="$1"

    if ! type poetry 2>&1 >/dev/null; then
        curl -sSL https://install.python-poetry.org | python3 -

        poetry completions zsh >"$whereami/_poetry"
    fi
}

install_poetry "${0:A:h}"
unset install_poetry
